({
    getWeather: function (component, evant, helper) {
        let cityName = component.find('inputCity').get('v.value');
        let action = component.get("c.getWeatherJson");
        if (cityName != null) {
            action.setParams({
                cityName: cityName
            });
        } else {
            let cityName = 'London';
            action.setParams({
                cityName: cityName
            });
        }
        action.setCallback(this, function (response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let result = JSON.parse(response.getReturnValue());
                let iconletiable = "http://openweathermap.org/img/w/" + result.weather[0].icon +".png";
                component.set("v.iconletiable", iconletiable);
                component.set("v.mydata", result);
            } else if (state === "ERROR") {
                let errors = response.getError();
                console.error(errors);
            }
        });
        $A.enqueueAction(action);
    }
})