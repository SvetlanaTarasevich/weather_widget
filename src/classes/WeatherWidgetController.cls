public class WeatherWidgetController {
    @AuraEnabled
    public static String getWeatherJson(String cityName) {
        String apiKey = 'dfca2079f10773c1af4e91f0b98d9c9a';
        HttpRequest req = new HttpRequest();
        String url = 'https://api.openweathermap.org/data/2.5/weather?q=' + cityName + '&units=metric&APPID=' + apiKey;
        req.setEndpoint(url);
        req.setMethod('GET');
        Http http = new Http();
        HttpResponse res = http.send(req);
        return res.getBody();
    }
}